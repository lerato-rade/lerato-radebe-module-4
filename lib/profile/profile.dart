import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: ListView(
        children: [
          const SizedBox(
            height: 25,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(label: Text("username")),
                  initialValue: "Lerato",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(
                    label: Text("name"),
                    enabled: false,
                  ),
                  initialValue: "Karabo",
                  enabled: false,
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(
                    label: Text("surname"),
                  ),
                  initialValue: "Radebe",
                  enabled: false,
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  decoration: const InputDecoration(label: Text("email")),
                  initialValue: "karabo05@gmail.com",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  obscureText: true,
                  decoration: const InputDecoration(label: Text("password")),
                  initialValue: "1234",
                ),
              ),
              SizedBox(
                width: 255,
                child: TextFormField(
                  obscureText: true,
                  decoration:
                      const InputDecoration(label: Text("confirm password")),
                  initialValue: "1234",
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: const Color.fromARGB(255, 135, 207, 235)),
                onPressed: (() => Navigator.pushNamed(context, "/home")),
                child: const Text("confirm"),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
