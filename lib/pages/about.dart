import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("About"),
        backgroundColor:  const Color.fromARGB(255, 135, 207, 235),
      ),
      body: const Center(
          child: Text(
        "Greetings my name is karabo and this is my app",
        style: TextStyle(color:  Color.fromARGB(255, 135, 207, 235),fontSize: 22),
      )),
    );
  }
}
