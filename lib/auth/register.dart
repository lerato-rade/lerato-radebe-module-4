import 'package:flutter/material.dart';

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text("Regiser"),
          ),
          const SizedBox(
            height: 25,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                width: 255,
                child: TextField(
                  decoration: InputDecoration(label: Text("username")),
                ),
              ),
              const SizedBox(
                width: 255,
                child: TextField(
                  decoration: InputDecoration(label: Text("name")),
                ),
              ),
              const SizedBox(
                width: 255,
                child: TextField(
                  decoration: InputDecoration(label: Text("surname")),
                ),
              ),
              const SizedBox(
                width: 255,
                child: TextField(
                  decoration: InputDecoration(label: Text("email")),
                ),
              ),
              const SizedBox(
                width: 255,
                child: TextField(
                  decoration: InputDecoration(label: Text("password")),
                ),
              ),
              const SizedBox(
                width: 255,
                child: TextField(
                  decoration: InputDecoration(label: Text("confirm password")),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: const Color.fromARGB(255, 135, 207, 235)),
                onPressed: (() => Navigator.pushNamed(context, "/")),
                child: const Text("Sign Up"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text("Already have an account?"),
                    InkWell(
                      child: const Text(
                        " login",
                        style: TextStyle(color: Color.fromARGB(255, 135, 207, 235)),
                      ),
                      onTap: () => {Navigator.pushNamed(context, "/")},
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
